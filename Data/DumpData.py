import csv
from datetime import datetime
from django.db.models import Q
from pytz import utc
from Article.models import Article
from Hashtag.models import TagOfArticle
from Tweet.models import Tweet
import json
from django.core import serializers

__author__ = 'claire'

def run():
    articles = Article.objects.filter(HaveHashtag=True).order_by('?')[:100]
    tweet_ids = [id for article in articles for id in filter(None,article.Tweets.split(','))]
    tweets = Tweet.objects.filter(TweetID__in=tweet_ids)
    article_ids = [article.ID for article in articles]
    hashtags = [TagOfArticle.objects.filter(Article__ID=article_id).last() for article_id in article_ids]

    JSON_serializer = serializers.get_serializer("json")
    json_serializer = JSON_serializer()
    with open('articles.json', 'w') as file:
        json_serializer.serialize(articles, stream=file)
        # json.dump(articles, file)

    with open('tweets.json', 'w') as file:
        json_serializer.serialize(tweets, stream=file)
        # json.dump(articles, file)

    with open('hashtagsForArticles.json', 'w') as file:
        json_serializer.serialize(hashtags, stream=file)
        # json.dump(articles, file)




articles1 = Article.objects.filter(Q(Headline__icontains='irish')|Q(SubHeadline__icontains='irish')).filter(Q(Headline__icontains='water')|Q(SubHeadline__icontains='water')).order_by('DateTime')
len(articles1)

articles2 = Article.objects.filter(Q(Headline__icontains='charges')|Q(SubHeadline__icontains='charges')).filter(Q(Headline__icontains='water')|Q(SubHeadline__icontains='water')).order_by('DateTime')
len(articles2)

articles = sorted(set(list(articles1)+list(articles2)),key=lambda x:x.DateTime)
len(articles)

with open('IrishWater_Articles_for_Label.csv','w') as CSV_file:
    for article in articles:
        writer = csv.writer(CSV_file, lineterminator='\n')
        writer.writerow([article.pk,'',article.Headline,article.SubHeadline,article.DateTime])
    CSV_file.close()


true_id = []
with open('IrishWater_Articles_for_Label.csv','r') as CSV_file:
    file = csv.reader(CSV_file,delimiter=',')
    for line in file:
        if line[1] == '1':
            true_id.append(line[0])
    CSV_file.close()

print(true_id)

th1 = datetime(2016, 10, 15, 0, 00,0000, tzinfo=utc)
th2 = datetime(2016, 10, 16, 0, 00,0000, tzinfo=utc)

article = Article.objects.filter(pk__in=true_id).only('ID','DateTime','Source','Story_related','Url','Headline','SubHeadline','Content','Keywords','Stream_Keywords').order_by('DateTime')
with open("IrishWater_Articles.json", "w") as out:
    serializers.serialize('json',article, fields=('ID','DateTime','Source','Story_related','Url','Headline','SubHeadline','Content','Keywords','Stream_Keywords'),stream=out)
    out.close()


tweets = Tweet.objects.filter(Hashtags__icontains='#watercharges').order_by('DateTime')
len(tweets)



articles1 = Article.objects.filter(Q(Headline__icontains='london')|Q(SubHeadline__icontains='london')).filter(Q(Headline__icontains='attack')|Q(SubHeadline__icontains='attack')).order_by('DateTime')
len(articles1)

articles2 = Article.objects.filter(Q(Headline__icontains='westminster')|Q(SubHeadline__icontains='westminster')).filter(Q(Headline__icontains='attack')|Q(SubHeadline__icontains='attack')).order_by('DateTime')
len(articles2)

articles = sorted(set(list(articles1)+list(articles2)),key=lambda x:x.DateTime)
len(articles)

with open('LondonAttack_Articles_for_Label.csv','w') as CSV_file:
    for article in articles:
        writer = csv.writer(CSV_file, lineterminator='\n')
        writer.writerow([article.pk,'',article.Headline,article.SubHeadline,article.DateTime])
    CSV_file.close()




articles1 = Article.objects.filter(Q(Headline__icontains='orly')|Q(SubHeadline__icontains='orly')).filter(Q(Headline__icontains='airport')|Q(SubHeadline__icontains='airport')).order_by('DateTime')
len(articles1)

articles2 = Article.objects.filter(Q(Headline__icontains='paris')|Q(SubHeadline__icontains='paris')).filter(Q(Headline__icontains='orly')|Q(SubHeadline__icontains='orly')).order_by('DateTime')
len(articles2)

articles = sorted(set(list(articles1)+list(articles2)),key=lambda x:x.DateTime)
len(articles)

with open('OrlyAirport_Articles_for_Label.csv','w') as CSV_file:
    for article in articles:
        writer = csv.writer(CSV_file, lineterminator='\n')
        writer.writerow([article.pk,'',article.Headline,article.SubHeadline,article.DateTime])
    CSV_file.close()




articles1 = Article.objects.filter(Q(Headline__icontains='trump')|Q(SubHeadline__icontains='trump')).filter(Q(Headline__icontains='healthcare')|Q(SubHeadline__icontains='healthcare')).order_by('DateTime')
len(articles1)

articles2 = Article.objects.filter(Q(Headline__icontains='health')|Q(SubHeadline__icontains='health')).filter(Q(Headline__icontains='trump')|Q(SubHeadline__icontains='trump')).order_by('DateTime')
len(articles2)

articles = sorted(set(list(articles1)+list(articles2)),key=lambda x:x.DateTime)
len(articles)


with open('TrumpHealthcare_Articles_for_Label.csv','w') as CSV_file:
    for article in articles:
        writer = csv.writer(CSV_file, lineterminator='\n')
        writer.writerow([article.pk,'',article.Headline,article.SubHeadline,article.DateTime])
    CSV_file.close()




articles1 = Article.objects.filter(Q(Headline__icontains='france')|Q(SubHeadline__icontains='france')).filter(Q(Headline__icontains='lille')|Q(SubHeadline__icontains='lille')).order_by('DateTime')
len(articles1)

articles2 = Article.objects.filter(Q(Headline__icontains='health')|Q(SubHeadline__icontains='health')).filter(Q(Headline__icontains='trump')|Q(SubHeadline__icontains='trump')).order_by('DateTime')
len(articles2)

articles = sorted(set(list(articles1)+list(articles2)),key=lambda x:x.DateTime)
len(articles)


with open('TrumpHealthcare_Articles_for_Label.csv','w') as CSV_file:
    for article in articles:
        writer = csv.writer(CSV_file, lineterminator='\n')
        writer.writerow([article.pk,'',article.Headline,article.SubHeadline,article.DateTime])
    CSV_file.close()


tweets = Tweet.objects.filter(Hashtags__icontains='#lille').order_by('DateTime')
len(tweets)